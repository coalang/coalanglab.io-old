# Specification

## Syntax

Coa's syntax will and should be very simple. 

### Seed

Every seed, also known as a program will have at least zero characters.

A seed (aka program) should have one and only one non-decorated block literal.

### Special Characters

These are the special characters:
- `.`
- `'`
- `"`
- `(`
- `)`
- `[`
- `]`
- whitespaces (a whitespace in any language, a space, tab, or newline)

### Object Literal

A object literal is a representation of a object inside a seed. Here are some examples, separated by newlines:

```
object_literal
parent.child
function(arguments)
```

A object literal is one of these literals:

- reference literal
- map literal
- block literal
- text literal
- computed literal
- comment literal

### Reference Literal

Work in progress.

Reference literals reference some object in runtime. Reference literals can be extended with other reference literals, separated by dots (`.`). Reference literals can contain any character except special characters.

```
object
parent.child
its_OK_to_have_these:_|\,<>/?;_and_more!
```

### Map Literal

Work in progress.

Map literals are representations of maps in a seed. They start with a open paren (`(`), and end with a close paren (`)`). A map contains object literals separated by whitespaces.

```
(
    object
    parent.child
    assignee.=(assigner)
    (
        another_map_inside!
    )
)
```

### Block Literal

Work in progress.

Block literals are representations of blocks in a seed. They start with a open brack[et] (`[`) and end with a close brack[et] (`]`). A block literal has zero or more object literals, and should be separated by whitespaces (in any language, a space, tab, or newline).

```
# This:
..dev.coa.core.io.out('Coa Version: "..dev.coa.core.ver.to.txt"')
# is the same as this:
io.out('Coa Version: "ver.to.txt"')
```

### Computed Literal

Work in progress.

Computed literals are enclosed in a quotation mark (`"`), and are computed and changed into object literals as if already existing in a program.

```
# This:
io.out('1+2="1.+(2)"')
# is the same as this:
io.out('1+2=3')
# and this:
"'i'.=('o')".out('hello')
# is the same as this:
io.out('hello')
```

### Comment Literal

Work in progress.

Comment literals are enclosed in a number sign (`#`) and a newline (in any language).
