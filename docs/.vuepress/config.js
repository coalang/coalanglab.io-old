module.exports = {
    title: 'Coa',
    description: 'A clean, object-oriented, and asynchronous programming language',
    head: [
        ['link', { rel: "icon", type: "image/png", sizes: "1000x1000", href: "/icon.png"}],
    ],
    base: '/',
    dest: 'public'
}
