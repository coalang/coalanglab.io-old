# Coa Roadmap

 - [ ] `dev.coa.compiler` Coa Compiler
	 - [x] `dev.coa.compiler.parser` Coa Compiler Parser/Lexer
	 - [x] `dev.coa.compiler.gen` Coa Compiler Source Code Generator
 - [ ] `dev.coa.impl.go` Coa Implementation in Go
 - [ ] `dev.coa.core` Coa Core
	 - [ ] `dev.coa.core.io` Coa Core IO
	 - [ ] `dev.coa.core.obj` Coa Core Object System
	 - [ ] `dev.coa.ips` Coa Core IP Suite
 - [ ] `dev.coa.skin` Coa Skin
	 - [ ] `dev.coa.skin.interactive` Coa Skin Interactive (like IPython)
 - [ ] `dev.coa.idx` Coa Index
 - [ ] `dev.coa.ide` Coa IDE

> Written with [StackEdit](https://stackedit.io/).
