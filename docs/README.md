---
home: true
heroImage: /icon.png
features:
- title: Clean & Consistent
  details: Coa is designed to be clean. It has a consistent interface.
- title: Object-oriented
  details: Coa is object-oriented and supports multiple other paradigms.
- title: Asynchronous
  details: Coa makes asynchronous programming easy.
footer: Test is licensed under the GNU Free Documentation License. Code portions are licensed under the GNU General Public License <>.
---

## Use other languages.

<pre>
</pre>


<table>
  <tr>
    <th>Coa 1</th>
    <td><pre>
fmt.=(...go.fmt)
fmt.Println('Hello, 世界')
    </pre></td>
  </tr>
  <tr>
    <th>Go 1</th>
    <td><pre>
package main

import "fmt"

func main() {
  fmt.Println("Hello, 世界")
}
    </pre></td>
  </tr>
  </tr>
</table>


## Similar (ish) to other languages.

```
// Coa
chain.=(
  links.=(.num..type 4)
)

chain..to.text.=([
  .re('I have "links" links')
])
```

```
// Go
type Chain struct {
  links int
}

func (c Chain) String() string {
  return fmt.Sprintf("I have %v links", c.links)
}
```

```
// TypeScript
class Chain {
  links: number = 4
  public toString() = () : string => {
    return `I have ${this.links} links`
  };
}
```
Coa stands for **C**oa is **o**bject-oriented and **a**synchronous (formerly **C**lean, **o**bject-oriented and **a**synchronous).

Here is what Coa looks like:
```
# Integers & arithmetic expressions...
version.=( 1.=( 50./(2) ).-( 8.*(3) ) )

# ... and strings
name.=('The Coa programming language')

# ... booleans
is_coa_object_oriented.=(.true)

# ... custom objects
person.=(.type(
  name.==(.==(.text.of))
  age.==(.==(.int.of))
))

# ... maps (which are lists and hash maps merged together)
people.=(
  person(name.=('Anna') age.=(24))
  person(name.=('Bob') age.=(99))
)
```

It also has functions (obviously)!

```
# User-defined functions...
person.get_name.=([
  .re(name)
])

people(0).get_name # 🡢 'Anna'
people(1).get_name # 🡢 'Bob'

# and built-in functions
.io.out(people.len) # print: 2
```

And Coa has great built in libraries, powered by either the Go or Julia standard libraries.

```
http.=(..coa.core.ips.http)
r.=(http.get('https://example.com'))
.io.out(r.re)
```
```
ssh.=(..coa.core.ips.ssh)
session.=(ssh.start('ssh://johndoe@example.com:22' 'password')).re
.io.out(session.send('ls -l').re)
```

Coa also has packages (called pods) to organise your code:

```
..dev.coa.core.io.out('is the same as:')
.io.out('this!')
```

## Packages

Packages in Coa are called pods, and they are structured like this:

```
coa_pod.pod (tar.gz file)
  info.toml
  program_0.coa
  program_1.coa
```

`info.toml`
```
[meta]
name = "Example Coa Pod"
desc = "This is an example Coa Pod."

[meta.author]
name = "John Doe"
email = "johndoe@example.com"
url = "johndoe.example.com"
```

## Execution

Coa is compiled into machine code using Go/Julia as an intermediate representation. Directly compiling to LLVM IR or GCC IR is a option or decreasing compile time. Also, since Coa is compiled into Go, it can be interpreted after compiling.

```mermaid
graph TD
  A(Coa Source Code) --> |"Coa (Coa2Go) Compiler"| B(Go Source Code)
  B --> |"Go Compiler (gccgo, gc)"| C(Machine Code)
```
## Future Roadmap
### Compiling
There are plans to make a Coa-to-C compiler. This enables Coa binaries to be made. since this requires a more complete (or more low-level) Coa Standard pod (`dev.coa.core`), this will take more time and is thus in the Future Roadmap section.

```mermaid
graph TD
  A(Coa Source Code) --> |"Coa (Coa2C) Compiler"| B(C Source Code)
  B --> |"C Compiler (gcc, llvm)"| C(Machine Code)
```

Other potential compiling pipelines may use C++, C#, Java, Dart, Swift as intermediate languages due to some of these being recommended for certain platforms.

### Target Platforms and Languages

Coa 1+.x.x will support what vanilla Go can support. 

Future versions may support these target languages, sorted by priority:

1. Go (supported in Coa 1+.x.x)
2. Python (3+.x.x)
3. ECMAScript (ES2020)
4. Kotlin, Java
5. C, C++
6. C# (.NET)
7. Ruby
8. Dart
9. Swift
10. LLVM IR, GCC IR (possibly)

Future versions may support these target platforms/frameworks, sorted by priority:

1. GNU/Linux (all)
2. Windows NT (all)
3. macOS (all)
4. Kubernetes/Docker (all, tailored support)
5. Android (Java)
6. React, React Native (ES2020)
7. Flutter (Dart)
8. iOS, tvOS, etc (Swift)

> Unicode characters from [Xah Lee's website (HTTP)](http://xahlee.info/comp/unicode_arrows.html).

> "Inspired" (half-copy-and-pasted) from the [Monkey Language Website](https://monkeylang.org/).

> Written with [StackEdit](https://stackedit.io/).
