# Tutorial

## Math

`1.+(1)`🡢`2`

`2.2√`🡢`1.414214...`

## Variables

`a.=(1)`🡢`a`

`a`🡢`1`

`b.=(2.*(3))`🡢`b`

`a.+(b)`🡢`7`

## Conditions

`a.=(2)`

`a.==(1).then([.io.out('a is one')]).!.then([.io.out('a is not one')])`🡢`a is not one`

`.io.out((.true.=('a is one') .false.=('a is not one'))(a.==(1)))`🡢`a is not one`

> More coming soon...

> **"Inspired"** from [Io's tutorial](https://iolanguage.org/tutorial).
